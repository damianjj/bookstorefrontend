import { Injectable } from '@angular/core';
import { Book } from './book';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private booksUrl = 'http://localhost:8081/books';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) {}

  getBooks(page: number, size: number): Observable<Book[]> {
    const url = `${this.booksUrl}?page=${page}&size=${size}`;
    return this.http.get<Book[]>(url).pipe(
      tap(() => console.log('books loaded')),
      catchError(this.handleError<Book[]>('getBooks', []))
    );
  }

  getBook(id: number): Observable<Book> {
    const url = `${this.booksUrl}/${id}`;
    return this.http.get<Book>(url).pipe(
      tap(_ => console.log(`got book w/ id=${id}`)),
      catchError(this.handleError<Book>('getBook id=${id}'))
    );
  }

  updateBook(book: Book): Observable<any> {
    return this.http.put(this.booksUrl, book, this.httpOptions).pipe(
      tap(_ => console.log(`updated book w/ id=${book.id}`)),
      catchError(this.handleError<any>('updateBook'))
    );
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.booksUrl, book, this.httpOptions).pipe(
      catchError(this.handleError<Book>('addBook'))
    );
  }

  deleteBook(book: Book | number): Observable<Book> {
    const id = typeof book === 'number' ? book : book.id;
    const url = `${this.booksUrl}/${id}`;
    return this.http.delete<Book>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted book w/ id=${id}`)),
      catchError(this.handleError<Book>('deleteBook'))
    );
  }

  searchBooks(term: string, property: string): Observable<Book[]> {
    if (!term.trim()) {
      return of([]);
    }

    return this.http
      .get<Book[]>(`${this.booksUrl}/?${property}=${term}`)
      .pipe(catchError(this.handleError<Book[]>('searchBooks', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
