import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  books: Book[];
  selectedBook: Book;
  searchControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  constructor(private bookService: BookService) {}

  ngOnInit() {
    this.options = [];
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks(0, 10).subscribe(
      books => (
        (this.books = books),
        books.forEach(book => {
          this.options.push(book.title);
          this.options.push(book.author);
          this.options.push(book.publicationDate);
        })
      )
    );

    this.filteredOptions = this.searchControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  sortBooksByTitle(): void {
    if (this.books) {
      this.books.sort((a, b) => a.title.localeCompare(b.title));
    }
  }

  sortBooksByAuthor(): void {
    if (this.books) {
      this.books.sort((a, b) => a.author.localeCompare(b.author));
    }
  }

  onSelect(book: Book, event: { target: { tagName: string } }): void {
    if (event.target.tagName === 'BUTTON') {
      return;
    }

    if (this.selectedBook == null) {
      this.selectedBook = book;
    } else if (this.selectedBook === book) {
      this.selectedBook = null;
    } else {
      this.selectedBook = book;
    }
  }

  searchBooks(term: string): void {
    if (!term.trim()) {
      this.ngOnInit();
    }

    this.books = [];
    this.bookService
      .searchBooks(term, 'title')
      .subscribe(books => books.forEach(book => this.books.push(book)));
    this.bookService
      .searchBooks(term, 'author')
      .subscribe(books => books.forEach(book => this.books.push(book)));
    this.bookService
      .searchBooks(term, 'publicationDate')
      .subscribe(books => books.forEach(book => this.books.push(book)));
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    this.options = this.options.filter(
      (item, index) => this.options.indexOf(item) === index
    );

    return this.options.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }
}
