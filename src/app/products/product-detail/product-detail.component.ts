import { Component, OnInit, Input } from '@angular/core';
import { Book } from '../../book';
import { BookService } from 'src/app/book.service';
import { ProductsComponent } from '../products.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  @Input() book: Book;

  constructor(private bookService: BookService, private productsConponent: ProductsComponent) {}

  ngOnInit() {}

  delete(book: Book): void {
    this.bookService.deleteBook(book).subscribe();
    this.productsConponent.ngOnInit();
  }
}
