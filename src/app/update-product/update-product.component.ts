import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BookService } from '../book.service';
import { Book } from '../book';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroupDirective,
  NgForm
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {
  id: string;
  book: Book;
  bookForm: any;
  matcher = new MyErrorStateMatcher();
  titleFormControl = new FormControl('', [Validators.required]);
  authorFormControl = new FormControl('', [Validators.required]);
  publicationFormControl = new FormControl('', [Validators.required]);

  constructor(
    private bookService: BookService,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.bookService
      .getBook(Number(this.id))
      .subscribe(
        book => (
          (this.book = book),
          (this.titleFormControl = new FormControl(this.book.title, [
            Validators.required
          ])),
          (this.authorFormControl = new FormControl(this.book.author, [
            Validators.required
          ])),
          (this.publicationFormControl = new FormControl(
            this.book.publicationDate,
            [Validators.required]
          ))
        )
      );
    this.bookForm = this.formBuilder.group({
      title: '',
      author: '',
      publication: ''
    });
  }

  update(id: number, title: string, author: string, publicationDate: string): void {
    title = title.trim();
    author = author.trim();
    publicationDate = publicationDate.trim();

    if (!title || !author || !publicationDate) {
      return;
    }

    this.bookService
      .updateBook({ id, title, author, publicationDate } as Book)
      .subscribe();

    this.router.navigateByUrl('/home');
  }
}
