import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from '../book.service';
import { Book } from '../book';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroupDirective,
  NgForm
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {
  bookForm: any;
  matcher = new MyErrorStateMatcher();
  titleFormControl = new FormControl('', [Validators.required]);
  authorFormControl = new FormControl('', [Validators.required]);
  publicationFormControl = new FormControl('', [Validators.required]);

  constructor(
    private bookService: BookService,
    private router: Router,
    formBuilder: FormBuilder
  ) {
    this.bookForm = formBuilder.group({
      title: '',
      author: '',
      publication: ''
    });
  }

  ngOnInit() {}

  add(title: string, author: string, publicationDate: string): void {
    title = title.trim();
    author = author.trim();
    publicationDate = publicationDate.trim();

    if (!title || !author || !publicationDate) {
      return;
    }

    this.bookService
      .addBook({ title, author, publicationDate } as Book)
      .subscribe();

    this.router.navigateByUrl('/home');
  }
}
